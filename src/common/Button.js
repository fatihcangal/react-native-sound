import React, {Component} from 'react';
import {Text, ToastAndroid, TouchableOpacity} from 'react-native';
import {initialArr} from "./Array";
var SoundPlayer = require('react-native-sound');
var song = null;
class Button extends Component {
    onPressButton(a) {
        song = new SoundPlayer(a, SoundPlayer.MAIN_BUNDLE, (error)=> {
            if (error) {
                ToastAndroid.show('error when init soundplayer', ToastAndroid.SHORT)
            }
            else {
                song.play((success) => {
                    if (!success) {
                        ToastAndroid.show('error when play soundplayer', ToastAndroid.SHORT)
                    }
                });
            }
        });
    }
    // onPressPauseButton() {
    //     if (song != null) {
    //         if (this.state.pause) {//play resume
    //             song.play((success) => {
    //                 if (!success) {
    //                     ToastAndroid.show('error when play soundplayer', ToastAndroid.SHORT)
    //                 }
    //             });
    //     }
    //       else song.pause();
    //       this.setState({pause: !this.state.pause });
    //
    //     }
    // }
    render(props) {
        const {buttonStyle, textStyle} = styles;
        return this.props.array.map((item,id) => {
            return (
                <TouchableOpacity
                    key={id}
                    onPress={this.onPressButton.bind(this,item.sound)}
                    style={buttonStyle}
                >
                    <Text style={textStyle}>
                        {item.text}
                    </Text>

                </TouchableOpacity>
            );
        });
    }
}
const styles = {
    buttonStyle:{
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius:5 ,
        borderWidth: 1,
        borderColor: '#007aaf',
        marginLeft: 5,
        marginRight:5,
        flexDirection: 'row'


    },
    textStyle:{
        alignSelf: 'center',
        justifyContent: 'center',
        //alignItems: 'center',
        color: '#007aaf',
        fontSize: 16,
        fontWeight: '600',
       // paddingTop: 10,
       // paddingBottom: 10,
        //flex: 1,
        paddingLeft:100,
        alignItems: 'center'


    }
};
export default Button;