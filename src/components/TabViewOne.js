import React from 'react';
import { View,ScrollView} from 'react-native';
import Button from "../common/Button";
import {initialArr} from "../common/Array";

const TabViewOne = () => {
const array = initialArr;
    const {tabStyle} = styles;
    return (
        <View style={tabStyle} >
            <ScrollView>
                <Button array={array}/>
            </ScrollView>
        </View>
    );
};
const styles = {
    tabStyle:{
        flex: 1,
        backgroundColor: '#ff4081',
    }
};
export default TabViewOne;