import React, {Component} from 'react';
import {View, Dimensions, Text, StyleSheet} from 'react-native';
import { TabView, TabBar, SceneMap,Scr } from 'react-native-tab-view';
import TabViewOne from "./TabViewOne";
import TabViewTwo from "./TabViewTwo";

const FirstRoute = () => (
    <TabViewOne/>
);
const SecondRoute = () => (
    <TabViewTwo/>
);
const ThirdRoute = () => (
    <TabViewTwo/>
);
const ForthRoute = () => (
    <TabViewTwo/>
);

class TabViews extends Component {
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'Anasayfa' },
            { key: 'second', title: 'Komikler' },
            { key: 'third', title: 'Derlemeler' },
            { key: 'forth', title: 'Miting' },
        ],
    };
    render () {
        return(
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: FirstRoute,
                        second: SecondRoute,
                        third: ThirdRoute,
                        forth: ForthRoute
                    })}
                    onIndexChange={index => this.setState({ index })}
                />
        );
    }
}
const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
});
export default TabViews;